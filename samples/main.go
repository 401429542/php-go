package main

import (
	php "gitee.com/shirdonl/php-go"
	"os"
)

func main() {
	engine, _ := php.New()

	context, _ := engine.NewContext()
	context.Output = os.Stdout

	context.Exec("index.php")
	engine.Destroy()
}
